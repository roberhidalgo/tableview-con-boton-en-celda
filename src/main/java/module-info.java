module es.progcipfpbatoi {
    requires javafx.controls;
    requires javafx.fxml;
    requires java.sql;

    opens es.progcipfpbatoi.controlador to javafx.fxml;
    opens es.progcipfpbatoi.modelo.dto to javafx.base;
    exports es.progcipfpbatoi;
}
