package es.progcipfpbatoi.modelo.dao;

import es.progcipfpbatoi.modelo.dto.Persona;

import java.util.List;

public interface PersonaDAOInterface {
    List<Persona> findAll();
}
