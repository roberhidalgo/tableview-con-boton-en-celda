package es.progcipfpbatoi.modelo.dao;

import es.progcipfpbatoi.modelo.dto.Persona;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.List;

public class InMemoryPersonaDAO implements PersonaDAOInterface{

    private ArrayList<Persona> personas;

    public InMemoryPersonaDAO() {
        this.personas = new ArrayList<>();
        init();
    }

    @Override
    public List<Persona> findAll() {
        return personas;
    }

    private void init() {
        this.personas.add(new Persona("Roberto", "666666666"));
        this.personas.add(new Persona("Alex", "666777666"));
    }
}
