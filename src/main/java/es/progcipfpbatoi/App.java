package es.progcipfpbatoi;

import es.progcipfpbatoi.controlador.*;
import es.progcipfpbatoi.modelo.dao.InMemoryPersonaDAO;
import javafx.application.Application;
import javafx.stage.Stage;

import java.io.IOException;

/**
 * Hello world!
 *
 */
public class App extends Application
{
    @Override
    public void start(Stage stage) throws IOException {
        InMemoryPersonaDAO personaDAO = new InMemoryPersonaDAO();
        GenericController controller = new PrincipalController(personaDAO);
        ChangeScene.change(stage, controller,"/vista/principal.fxml", 400, 400);
    }

    public static void main(String[] args) {
        launch();
    }
}
