package es.progcipfpbatoi.controlador;

import es.progcipfpbatoi.modelo.dto.Persona;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.Button;
import javafx.scene.control.TableCell;
import javafx.scene.layout.AnchorPane;

import java.io.IOException;

public class ButtonsCellController extends TableCell<Persona, Void> {
    @FXML
    private AnchorPane root;

    @FXML
    private Button boton;

    public ButtonsCellController() {

        FXMLLoader loader = new FXMLLoader(getClass().getResource("/vista/buttons_cell.fxml"));
        loader.setController(this);

        try {
            loader.load();
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }

    @Override
    public void updateItem(Void item, boolean empty) {
        super.updateItem(item, empty);
        if (empty) {
            setGraphic(null);
        } else {
            setGraphic(root);
        }
    }

    @FXML
    private void handleButton(ActionEvent event) {

        Persona personaAEditar = getTableView().getItems().get(getIndex());
        System.out.println("Botón de " + personaAEditar + " pulsado");
    }

}
