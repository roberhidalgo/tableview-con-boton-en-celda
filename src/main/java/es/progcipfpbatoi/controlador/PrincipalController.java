package es.progcipfpbatoi.controlador;

import es.progcipfpbatoi.modelo.dao.PersonaDAOInterface;
import es.progcipfpbatoi.modelo.dto.Persona;
import javafx.fxml.FXML;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.AnchorPane;


import java.util.ArrayList;
import java.util.List;

public class PrincipalController implements GenericController{

    private PersonaDAOInterface personaDAO;
    @FXML TableColumn nombreCol;
    @FXML
    TableColumn telefonoCol;
    @FXML
    TableColumn botonCol;

    public PrincipalController(PersonaDAOInterface userDAO) {
        this.personaDAO = userDAO;
    }

    @FXML
    private TableView<Persona> tableViewPersonas;

    @FXML
    private AnchorPane root;

    @Override
    public void initialize() {
        tableViewPersonas.getItems().addAll(getData());
        nombreCol.setCellValueFactory(new PropertyValueFactory("nombre"));
        telefonoCol.setCellValueFactory(new PropertyValueFactory("telefono"));
        botonCol.setCellFactory(param -> new ButtonsCellController());
    }

    private List<Persona> getData() {
            return this.personaDAO.findAll();
    };
}
